// import { inject, toRaw, onBeforeUnmount, provide } from 'vue'
import { inject, toRaw, provide, onBeforeUnmount } from 'vue'
import { watchSource } from './watch.js'
import { defineId } from '@/shared/uid.js'

// returns props without falsy values and id
const optionsSource = (type, props) => {
  return Object.entries(toRaw(props))
    .reduce((acc, prop) => {
      if (prop[0] !== 'id' && prop[1]) acc[prop[0]] = prop[1]
      return acc
    }, { type })
}

export const setupSource = (type, opts, props) => {
  // inject
  const map = inject('map')

  // set options
  const id = defineId(props)
  const options = optionsSource(type, props)

  // add to map
  map.value.addSource(id, options)
  const source = map.value.getSource(id)

  // watch props
  watchSource(type, id, opts, props, map)

  // clean
  onBeforeUnmount(() => {
    if (map && map.value) {
      const layers = map.value.getStyle().layers.filter(l => l.source === id)
      for (const l of layers) map.value.removeLayer(l.id)
      map.value.removeSource(id)
    }
  })

  // expose to children and parent
  provide('source', id)

  // to expose to parent
  return {
    map,
    id,
    source
  }
}
