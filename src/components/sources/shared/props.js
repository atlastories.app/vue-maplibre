import {
  optionsSourceScheme,
  optionsSourceEnding
} from '@/shared/options.js'

// prop for all layers
export const propsSource = {
  id: {
    type: String
  }
}

// prop for 'vector', 'raster', 'raster-dem' and 'geojson'
export const propsSourceAttribution = {
  attribution: {
    type: String
  }
}

// prop for 'vector' and 'geojson'
export const propsSourcePromoteId = {
  promoteId: {
    type: [String, Object]
  }
}

// props for 'vector', 'raster' and 'raster-dem'
export const propsSourceTiles = {
  bounds: {
    type: Array,
    default: () => [-180, -85.051129, 180, 85.051129]
  },
  maxzoom: {
    type: Number // ,
    // default: 0
  },
  minzoom: {
    type: Number // ,
    // default: 22
  },
  tiles: {
    type: Array
  },
  url: {
    type: String
  },
  volatile: {
    type: Boolean
  }
}

// props for 'image' and 'video'
export const propsSourceBitmap = {
  coordinates: {
    type: Array,
    required: true
  }
}

export const propsSourceImage = {
  url: {
    type: String,
    required: true
  }
}

export const propsSourceVideo = {
  urls: {
    type: Array,
    required: true
  }
}

// props for 'vector' and 'raster'
export const propsSourceScheme = {
  scheme: {
    type: String,
    default: optionsSourceScheme[0],
    validator: s => optionsSourceScheme.includes(s)
  }
}

export const propsSourceGeojson = {
  buffer: {
    type: Number,
    default: 128,
    validator: n => n >= 0 && n <= 512
  },
  cluster: {
    type: Boolean
  },
  clusterMaxZoom: {
    type: Number
  },
  clusterMinPoints: {
    type: Number
  },
  clusterProperties: {
    type: Object
  },
  clusterRadius: {
    type: Number,
    default: 50,
    validator: n => n >= 0
  },
  data: {
    type: [String, Object]
  },
  filter: {
    type: Object
  },
  generateId: {
    type: Boolean
  },
  lineMetrics: {
    type: Boolean
  },
  maxzoom: {
    type: Number,
    default: 18
  },
  tolerance: {
    type: Number,
    default: 0.375
  }
}

export const propsSourceTileSize = {
  tileSize: {
    type: Number,
    default: 512
  }
}

export const propsSourceEncoding = {
  encoding: {
    type: String,
    default: optionsSourceEnding[1],
    validator: s => optionsSourceEnding.includes(s)
  }
}
