import { watch } from 'vue'

export const watchSource = (type, id, opts, props, map) => {
  // method to get source
  const source = () => map.value.getSource(id)

  // watch props that can be updated
  if (opts.data) {
    watch(() => props.data, val => {
      source().setData(val)
    })
  }
  if (opts.coordinates) {
    watch(() => props.coordinates, val => {
      if (val) source().setCoordinates(val)
    })
  }
  if (opts.url && type === 'image') {
    watch(() => props.url, val => {
      if (val) source().updateImage({ url: val })
    })
  }
}
