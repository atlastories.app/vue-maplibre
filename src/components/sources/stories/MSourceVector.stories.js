import MMap from '@/components/map/MMap.vue'
import MSourceVector, { opts } from '@/components/sources/MSourceVector.vue'
import MLayerLine from '@/components/layers/MLayerLine.vue'
import { defineArgs } from '@/shared/args.js'

export default {
  title: 'Sources/MSourceVector',
  component: MSourceVector,
  argTypes: defineArgs(opts)
}

export const Template = args => ({
  components: {
    MMap,
    MSourceVector,
    MLayerLine
  },
  setup () {
    return {
      args
    }
  },
  template: `
  <m-map
    ref="map"
    tiles="https://api.maptiler.com/maps/streets/style.json?key=get_your_own_OpIi9ZULNHzrESv6T2vL"
    :center="[-122.447303, 37.753574]"
    :zoom="13">

    <m-source-vector
      v-bind="args">

      <m-layer-line
        sourceLayer="contour"
        join="round"
        cap="round"
        color="#ff69b4"
        :width="1" />

    </m-source-vector>

  </m-map>`
})

export const Example = Template.bind({})
Example.args = {
  url: 'https://api.maptiler.com/tiles/contours/tiles.json?key=get_your_own_OpIi9ZULNHzrESv6T2vL'
}
