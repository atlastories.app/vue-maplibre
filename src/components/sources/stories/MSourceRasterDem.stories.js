import MMap from '@/components/map/MMap.vue'
import MSourceRasterDem, { opts } from '@/components/sources/MSourceRasterDem.vue'
import { defineArgs } from '@/shared/args.js'

export default {
  title: 'Sources/MSourceRasterDem',
  component: MSourceRasterDem,
  argTypes: defineArgs(opts)
}

const Template = args => ({
  components: {
    MMap,
    MSourceRasterDem
  },
  setup () {
    return { args }
  },
  template: `
  <m-map
    tiles="https://api.maptiler.com/maps/streets/style.json?key=get_your_own_OpIi9ZULNHzrESv6T2vL"
    :center="[11.39085, 47.27574]"
    :zoom="12"
    :pitch="52"
    :tiles="style"
    :maxZoom="18"
    :maxPitch="85">

    <m-source-raster-dem
      v-bind="args" />

  </m-map>`
})

export const Example = Template.bind({})
Example.args = {
  url: 'https://demotiles.maplibre.org/terrain-tiles/tiles.json',
  tileSize: 256
}

const TemplateSources = () => ({
  components: {
    MMap,
    MSourceRasterDem
  },
  template: `
  <m-map
    tiles="https://api.maptiler.com/maps/streets/style.json?key=get_your_own_OpIi9ZULNHzrESv6T2vL"
    :center="[11.39085, 47.27574]"
    :zoom="12"
    :pitch="52"
    :maxZoom="18"
    :maxPitch="85">

    <m-source-raster-dem
      url="https://demotiles.maplibre.org/terrain-tiles/tiles.json"
      :tileSize="256" />

  </m-map>`
})

export const Source = TemplateSources.bind({})
