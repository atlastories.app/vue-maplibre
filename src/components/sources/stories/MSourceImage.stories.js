import MMap from '@/components/map/MMap.vue'
import MSourceImage, { opts } from '@/components/sources/MSourceImage.vue'
import MLayerRaster from '@/components/layers/MLayerRaster.vue'
import { defineArgs } from '@/shared/args.js'

export default {
  title: 'Sources/MSourceImage',
  component: MSourceImage,
  argTypes: defineArgs(opts)
}

const Template = args => ({
  components: {
    MMap,
    MSourceImage,
    MLayerRaster
  },
  setup () {
    return {
      args
    }
  },
  template: `
  <m-map
    ref="map"
    tiles="https://api.maptiler.com/maps/streets/style.json?key=get_your_own_OpIi9ZULNHzrESv6T2vL"
    :center="[ -74.97761625750599, 42.3689896788369 ]"
    :zoom="5">

    <m-source-image
      :url="args.url"
      :coordinates="args.coordinates" />

  </m-map>`
})

export const Example = Template.bind({})
Example.args = {
  url: 'https://atlastories.gitlab.io/vue-maplibre/radar.gif',
  coordinates: [
    [-80.425, 46.437],
    [-71.516, 46.437],
    [-71.516, 37.936],
    [-80.425, 37.936]
  ]
}
