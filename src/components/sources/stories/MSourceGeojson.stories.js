import MMap from '@/components/map/MMap.vue'
import MPopup from '@/components/map/MPopup.vue'
import MSourceGeojson, { opts } from '@/components/sources/MSourceGeojson.vue'
import MLayerFill from '@/components/layers/MLayerFill.vue'
import MLayerCircle from '@/components/layers/MLayerCircle.vue'
import MLayerSymbol from '@/components/layers/MLayerSymbol.vue'
import MLayerSymbolText from '@/components/layers/MLayerSymbolText.vue'
import { defineArgs } from '@/shared/args.js'
import { ref, reactive } from 'vue'

export default {
  title: 'Sources/MSourceGeojson',
  component: MSourceGeojson,
  argTypes: defineArgs(opts)
}

const geojson = {
  type: 'Feature',
  geometry: {
    type: 'Polygon',
    coordinates: [
      [
        [-67.13734351262877, 45.137451890638886],
        [-66.96466, 44.8097],
        [-68.03252, 44.3252],
        [-69.06, 43.98],
        [-70.11617, 43.68405],
        [-70.64573401557249, 43.090083319667144],
        [-70.75102474636725, 43.08003225358635],
        [-70.79761105007827, 43.21973948828747],
        [-70.98176001655037, 43.36789581966826],
        [-70.94416541205806, 43.46633942318431],
        [-71.08482, 45.3052400000002],
        [-70.6600225491012, 45.46022288673396],
        [-70.30495378282376, 45.914794623389355],
        [-70.00014034695016, 46.69317088478567],
        [-69.23708614772835, 47.44777598732787],
        [-68.90478084987546, 47.184794623394396],
        [-68.23430497910454, 47.35462921812177],
        [-67.79035274928509, 47.066248887716995],
        [-67.79141211614706, 45.702585354182816],
        [-67.13734351262877, 45.137451890638886]
      ]
    ]
  }
}

const Template = args => ({
  components: {
    MMap,
    MSourceGeojson,
    MLayerFill
  },
  setup () {
    return {
      args,
      geojson
    }
  },
  template: `
  <MMap
    tiles="https://api.maptiler.com/maps/streets/style.json?key=get_your_own_OpIi9ZULNHzrESv6T2vL"
    :center="[-68.13734351262877, 45.137451890638886]"
    :zoom="5">

    <MSourceGeojson
      :data="args.data">

      <MLayerFill
        color="#088"
        :opacity=".8 "/>

    </MSourceGeojson>
  </MMap>`
})

export const Example = Template.bind({})
Example.args = {
  data: geojson
}

export const Layers = () => ({
  components: {
    MMap,
    MSourceGeojson,
    MLayerCircle,
    MLayerFill
  },
  setup () {
    const geojson = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [-121.353637, 40.584978],
                [-121.284551, 40.584758],
                [-121.275349, 40.541646],
                [-121.246768, 40.541017],
                [-121.251343, 40.423383],
                [-121.32687, 40.423768],
                [-121.360619, 40.43479],
                [-121.363694, 40.409124],
                [-121.439713, 40.409197],
                [-121.439711, 40.423791],
                [-121.572133, 40.423548],
                [-121.577415, 40.550766],
                [-121.539486, 40.558107],
                [-121.520284, 40.572459],
                [-121.487219, 40.550822],
                [-121.446951, 40.56319],
                [-121.370644, 40.563267],
                [-121.353637, 40.584978]
              ]
            ]
          }
        },
        {
          type: 'Feature',
          geometry: {
            type: 'Point',
            coordinates: [-121.415061, 40.506229]
          }
        },
        {
          type: 'Feature',
          geometry: {
            type: 'Point',
            coordinates: [-121.505184, 40.488084]
          }
        },
        {
          type: 'Feature',
          geometry: {
            type: 'Point',
            coordinates: [-121.354465, 40.488737]
          }
        }
      ]
    }
    return {
      geojson
    }
  },
  template: `
  <m-map
    tiles="https://api.maptiler.com/maps/streets/style.json?key=get_your_own_OpIi9ZULNHzrESv6T2vL"
    :center="[-121.403732, 40.492392]"
    :zoom="10">

    <m-source-geojson
      :data="geojson">

      <m-layer-circle
        id="points"
        :filter="['==', '$type', 'Point']"
        :radius="6"
        color="#B42222" />

      <m-layer-fill
        before="points"
        :filter="['==', '$type', 'Polygon']"
        color="#888888"
        :opacity="0.4" />

    </m-source-geojson>

  </m-map>`
})

const TemplateCluster = () => ({
  components: {
    MMap,
    MPopup,
    MSourceGeojson,
    MLayerCircle,
    MLayerSymbol,
    MLayerSymbolText
  },
  setup () {
    // refs to access map and cluster instances
    const refmap = ref(null)
    const refcluster = ref(null)

    // map state
    const camera = reactive({
      center: [-103.59179687498357, 40.66995747013945],
      zoom: 3
    })
    const popup = reactive({
      lngLat: undefined,
      content: ''
    })

    // methods
    const clickCluster = e => {
      const features = refmap.value.map.queryRenderedFeatures(e.point)
      const group = features[0]
      camera.center = group.geometry.coordinates
      const clusterid = group.properties.cluster_id
      if (clusterid) {
        refcluster.value.source.getClusterExpansionZoom(
          clusterid,
          (err, zoom) => {
            if (err) return
            camera.zoom = zoom
          })
      }
    }
    const clickPoint = e => {
      const features = refmap.value.map.queryRenderedFeatures(e.point)
      const point = features[0]
      const tsunami = point.properties.tsunami ? 'yes' : 'no'
      popup.content = 'magnitude: ' + point.properties.mag + '<br>Was there a tsunami?: ' + tsunami
      popup.lngLat = point.geometry.coordinates
    }
    const closePopup = () => {
      popup.lngLat = undefined
      popup.content = ''
    }
    const mouseenter = () => {
      refmap.value.map.getCanvas().style.cursor = 'pointer'
    }
    const mouseleave = () => {
      refmap.value.map.getCanvas().style.cursor = ''
    }
    return {
      // refs
      refmap,
      refcluster,
      // state
      camera,
      popup,
      // methods
      mouseenter,
      mouseleave,
      clickCluster,
      clickPoint,
      closePopup
    }
  },
  template: `
  <m-map
    ref="refmap"
    tiles="https://api.maptiler.com/maps/streets/style.json?key=get_your_own_OpIi9ZULNHzrESv6T2vL"
    :animation="{ duration: 300 }"
    v-model:center="camera.center"
    v-model:zoom="camera.zoom">

    <m-source-geojson
      ref="refcluster"
      cluster
      data="https://atlastories.gitlab.io/vue-maplibre/earthquakes.geojson"
      :clusterMaxZoom="14"
      :clusterRadius="50">

      <m-layer-circle
        @click="clickCluster"
        @mouseenter="mouseenter"
        @mouseleave="mouseleave"
        :filter="['has', 'point_count']"
        :color="[
          'step', ['get', 'point_count'],
          '#51bbd6', 100,
          '#f1f075', 750,
          '#f28cb1'
        ]"
        :radius="[
          'step', ['get', 'point_count'],
          20, 100,
          30, 750,
          40
        ]" />

      <m-layer-symbol
        :filter="['has', 'point_count']">
        <m-layer-symbol-text
          field="{point_count_abbreviated}"
          :font="['DIN Offc Pro Medium', 'Arial Unicode MS Bold']"
          :size="12" />
      </m-layer-symbol>

      <m-layer-circle
        @click="clickPoint"
        @mouseenter="mouseenter"
        @mouseleave="mouseleave"
        :filter="['!', ['has', 'point_count']]"
        color="#11b4da"
        :radius="4"
        :strokeWidth="1"
        strokeColor="#fff" />

    </m-source-geojson>

    <m-popup
      @close="closePopup"
      v-if="popup.lngLat"
      :lngLat="popup.lngLat"
      v-html="popup.content" />

  </m-map>`
})

export const Cluster = TemplateCluster.bind({})
