import { ref, onMounted } from 'vue'
import MMap from '@/components/map/MMap.vue'
import MSourceVideo, { opts } from '@/components/sources/MSourceVideo.vue'
import MLayerRaster from '@/components/layers/MLayerRaster.vue'
import { defineArgs } from '@/shared/args.js'

export default {
  title: 'Sources/MSourceVideo',
  component: MSourceVideo,
  argTypes: defineArgs(opts)
}

const Template = args => ({
  components: {
    MMap,
    MSourceVideo,
    MLayerRaster
  },
  setup () {
    const video = ref(null)
    const playing = ref(true)

    const click = () => {
      if (playing.value) video.value.source.pause()
      else video.value.source.play()
      playing.value = !playing.value
    }

    return {
      args,
      video,
      click
    }
  },
  template: `
  <m-map
    ref="map"
    tiles="https://api.maptiler.com/maps/streets/style.json?key=get_your_own_OpIi9ZULNHzrESv6T2vL"
    :center="[-122.514426, 37.562984]"
    :bearing="-96"
    :zoom="17"
    @click="click">

    <m-source-video
      ref="video"
      v-bind="args" />

  </m-map>`
})

export const Example = Template.bind({})
Example.args = {
  urls: [
    'https://static-assets.mapbox.com/mapbox-gl-js/drone.mp4',
    'https://static-assets.mapbox.com/mapbox-gl-js/drone.webm'
  ],
  coordinates: [
    [-122.51596391201019, 37.56238816766053],
    [-122.51467645168304, 37.56410183312965],
    [-122.51309394836426, 37.563391708549425],
    [-122.51423120498657, 37.56161849366671]
  ]
}
