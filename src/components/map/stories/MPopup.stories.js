import MMap from '@/components/map/MMap.vue'
import MMarker from '@/components/map/MMarker.vue'
import MPopup, { opts } from '@/components/map/MPopup.vue'
import { defineArgs } from '@/shared/args.js'
import { eventsPopup } from '../shared/events.js'

export default {
  title: 'Map/MPopup',
  component: MPopup,
  argTypes: defineArgs(opts, eventsPopup)
}

const Template = args => ({
  components: {
    MMap,
    MPopup
  },
  setup () {
    return {
      args
    }
  },
  template: `
  <m-map
    tiles="https://api.maptiler.com/maps/streets/style.json?key=get_your_own_OpIi9ZULNHzrESv6T2vL"
    :center="[12.550343, 55.665957]"
    :zoom="8">

    <m-popup
      v-bind="args">
      My Popup content
    </m-popup>

  </m-map>`
})

export const Example = Template.bind({})
Example.args = {
  lngLat: [12.550343, 55.665957]
}

const TemplateMarker = args => ({
  components: {
    MMap,
    MMarker,
    MPopup
  },
  setup () {
    return {
      args
    }
  },
  template: `
  <m-map
    tiles="https://api.maptiler.com/maps/streets/style.json?key=get_your_own_OpIi9ZULNHzrESv6T2vL"
    :center="[12.550343, 55.665957]"
    :zoom="8">
    <m-marker
      :lngLat="[12.550343, 55.665957]"
      anchor="bottom">
      <template #popup>

        <m-popup
          v-bind="args">
          My Popup content
        </m-popup>

      </template>
    </m-marker>
  </m-map>`
})

export const Marker = TemplateMarker.bind({})
Marker.args = {}
