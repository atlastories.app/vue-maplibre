import MMap from '@/components/map/MMap.vue'
import MMarker, { opts } from '@/components/map/MMarker.vue'
import MPopup from '@/components/map/MPopup.vue'
import { defineArgs } from '@/shared/args.js'
import { argsEventsMarker } from '../shared/events.js'
import { ref } from 'vue'

export default {
  title: 'Map/MMarker',
  component: MMarker,
  argTypes: defineArgs(opts, argsEventsMarker)
}

const Template = args => ({
  components: {
    MMap,
    MMarker
  },
  setup () {
    return {
      args
    }
  },
  template: `
  <m-map
    tiles="https://api.maptiler.com/maps/streets/style.json?key=get_your_own_OpIi9ZULNHzrESv6T2vL"
    :center="[12.550343, 55.665957]"
    :zoom="8">

    <m-marker
      v-bind="args" />

  </m-map>`
})

export const Example = Template.bind({})
Example.args = {
  lngLat: [12.550343, 55.665957],
  anchor: 'bottom'
}

const TemplateAdd = () => ({
  components: {
    MMap,
    MMarker
  },
  template: `
  <m-map
    tiles="https://api.maptiler.com/maps/streets/style.json?key=get_your_own_OpIi9ZULNHzrESv6T2vL"
    :center="[12.550343, 55.665957]"
    :zoom="8">

    <m-marker :lngLat="[12.550343, 55.665957]" />

  </m-map>`
})

export const Add = TemplateAdd.bind({})


const TemplateEvents = () => ({
  components: {
    MMap,
    MMarker
  },
  setup () {
    // data
    const position = ref([12.550343, 55.665957])
    const draggable = ref(true)
    const dragging = ref(false)
    // methods
    const toggle = e => {
      draggable.value = !draggable.value
      console.log(e)
    }
    const dragstart = () => {
      dragging.value = true
    }
    const dragend = e => {
      position.value = e.target.getLngLat().toArray()
      dragging.value = false
      console.log(e)
    }
    return {
      position,
      draggable,
      dragging,
      toggle,
      dragstart,
      dragend
    }
  },
  template: `
<pre>{{ position }}</pre>
<pre>draggable: {{ draggable }}</pre>
<pre>dragging: {{ dragging }}</pre>

<m-map
  style="height:60%"
  tiles="https://api.maptiler.com/maps/streets/style.json?key=get_your_own_OpIi9ZULNHzrESv6T2vL"
  :center="[12.550343, 55.665957]"
  :zoom="8">

  <m-marker
    :draggable="draggable"
    :lngLat="position"
    @click="toggle"
    @dragstart="dragstart"
    @dragend="dragend" />

</m-map>`
})

export const Events = TemplateEvents.bind({})

const TemplateCustom = () => ({
  components: {
    MMap,
    MMarker
  },
  template: `
<m-map
  tiles="https://api.maptiler.com/maps/streets/style.json?key=get_your_own_OpIi9ZULNHzrESv6T2vL"
  :center="[12.550343, 55.665957]"
  :zoom="8">

  <m-marker
    :lngLat="[12.550343, 55.665957]">
    <img
      src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Cat_Laptop_-_Idil_Keysan_-_Wikimedia_Giphy_stickers_2019.gif/240px-Cat_Laptop_-_Idil_Keysan_-_Wikimedia_Giphy_stickers_2019.gif"
      width="120"
      height="120">
  </m-marker>

</m-map>`
})

export const Custom = TemplateCustom.bind({})

const TemplatePopup = () => ({
  components: {
    MMap,
    MMarker,
    MPopup
  },
  template: `
  <m-map
    tiles="https://api.maptiler.com/maps/streets/style.json?key=get_your_own_OpIi9ZULNHzrESv6T2vL"
    :center="[12.550343, 55.665957]"
    :zoom="8">

    <m-marker
      :lngLat="[12.550343, 55.665957]">
      <template #popup>

        <m-popup>
          <img
            style="float:left; margin-right:5px"
            src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Cat_Laptop_-_Idil_Keysan_-_Wikimedia_Giphy_stickers_2019.gif/240px-Cat_Laptop_-_Idil_Keysan_-_Wikimedia_Giphy_stickers_2019.gif"
            width="70"
            height="70">
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
        </m-popup>

      </template>
    </m-marker>

  </m-map>`
})

export const Popup = TemplatePopup.bind({})
