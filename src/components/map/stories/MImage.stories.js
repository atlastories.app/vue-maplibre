import MMap from '@/components/map/MMap.vue'
import MSourceGeojson from '@/components/sources/MSourceGeojson.vue'
import MImage, { opts } from '@/components/map/MImage.vue'
import MLayerSymbol from '@/components/layers/MLayerSymbol.vue'
import MLayerSymbolIcon from '@/components/layers/MLayerSymbolIcon.vue'
import { defineArgs } from '@/shared/args.js'
import { eventsImageEmit } from '../shared/events.js'

export default {
  title: 'Map/MImage',
  component: MImage,
  argTypes: defineArgs(opts, eventsImageEmit)
}

const img = 'https://upload.wikimedia.org/wikipedia/commons/7/7c/201408_cat.png'

const Template = args => ({
  components: {
    MMap,
    MImage,
    MSourceGeojson,
    MLayerSymbol,
    MLayerSymbolIcon
  },
  setup () {
    const geojson = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          geometry: {
            type: 'Point',
            coordinates: [0, 0]
          }
        }
      ]
    }
    return {
      img,
      args,
      geojson
    }
  },
  template: `
  <m-map
    tiles="https://api.maptiler.com/maps/streets/style.json?key=get_your_own_OpIi9ZULNHzrESv6T2vL">

    <m-image
      :src="args.src">

      <m-source-geojson
        :data="geojson">
        <m-layer-symbol>
          <m-layer-symbol-icon
            :image="args.src"
            :size="0.25" />
        </m-layer-symbol>
      </m-source-geojson>

    </m-image>
  </m-map>`
})

export const Example = Template.bind({})
Example.args = {
  src: img
}
