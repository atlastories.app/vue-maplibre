import { reactive, ref } from 'vue'
import MMap, { opts } from '@/components/map/MMap.vue'
import { defineArgs } from '@/shared/args.js'

export default {
  title: 'Map/MMap',
  component: MMap,
  argTypes: defineArgs(opts)
}

const Template = args => ({
  components: { MMap },
  setup () {
    return {
      args
    }
  },
  template: `<m-map v-bind="args" />`
})

export const Example = Template.bind({})
Example.args = {
  tiles: 'https://demotiles.maplibre.org/style.json'
}

export const Satellite = Template.bind({})
Satellite.args = {
  tiles: 'https://api.maptiler.com/maps/hybrid/style.json?key=get_your_own_OpIi9ZULNHzrESv6T2vL',
  center: [137.9150899566626, 36.25956997955441],
  zoom: 9
}

export const Tiles = () => ({
  components: {
    MMap
  },
  setup () {
    // constants
    const tilesDemo = 'https://demotiles.maplibre.org/style.json'
    const tilesSatellite = 'https://api.maptiler.com/maps/hybrid/style.json?key=get_your_own_OpIi9ZULNHzrESv6T2vL'
    const tilesStamen = {
      version: 8,
      sources: {
        'raster-tiles': {
          type: 'raster',
          tiles: [ 'https://stamen-tiles.a.ssl.fastly.net/watercolor/{z}/{x}/{y}.jpg' ],
          tileSize: 256,
          attribution: 'Map tiles by <a target="_top" rel="noopener" href="http://stamen.com">Stamen Design</a>, under <a target="_top" rel="noopener" href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a>. Data by <a target="_top" rel="noopener" href="http://openstreetmap.org">OpenStreetMap</a>, under <a target="_top" rel="noopener" href="http://creativecommons.org/licenses/by-sa/3.0">CC BY SA</a>'
        }
      },
      layers: [
        {
          id: 'simple-tiles',
          type: 'raster',
          source: 'raster-tiles',
          minzoom: 0,
          maxzoom: 22
        }
      ]
    }
    // data
    const tiles = ref(tilesDemo)
    // methods
    const setTiles = string => { tiles.value = string }

    return {
      tilesDemo,
      tilesSatellite,
      tilesStamen,
      setTiles,
      tiles
    }
  },
  template: `
<button @click="setTiles(tilesSatellite)">Satellite</button>
<button @click="setTiles(tilesStamen)">Watercolor</button>
<button @click="setTiles(tilesDemo)">Demo tiles</button>

<m-map :tiles="tiles" style="height:95%" />`
})

const TemplateVModel = () => ({
  components: {
    MMap
  },
  setup () {
    // data
    const camera = reactive({
      center: [4.65, 46],
      zoom: 3,
      pitch: 45,
      bearing: -20
    })
    // methods
    const setCenter = lngLat => {
      camera.center = lngLat
    }
    const zoomIn = () => {
      if (camera.zoom < 24) camera.zoom++
    }
    return {
      setCenter,
      zoomIn,
      camera
    }
  },
  template: `
<button @click="zoomIn">Zoom</button>
<button @click="setCenter([2.349014, 48.864716])">Paris</button>
<button @click="setCenter([-3.703790, 40.416775])">Madrid</button>

<pre>{{ camera }}</pre>

<div style="height:60%">
  <m-map
    v-model:center="camera.center"
    v-model:zoom="camera.zoom"
    v-model:pitch="camera.pitch"
    v-model:bearing="camera.bearing"
    tiles="https://demotiles.maplibre.org/style.json" />
</div>`
})

export const VModel = TemplateVModel.bind({})

const TemplateAnimation = () => ({
  components: {
    MMap
  },
  setup () {
    // data
    const camera = reactive({
      center: [4.65, 46],
      zoom: 3,
      pitch: 45,
      bearing: -20
    })
    const france = {
      center: [5.52, 46],
      zoom: 4,
      pitch: 55,
      bearing: -30
    }
    const spain = {
      center: [-3.12, 38.3],
      zoom: 4.5,
      pitch: 50,
      bearing: 20
    }
    // methods
    const setCamera = o => {
      for (const k of Object.keys(o)) {
        camera[k] = o[k]
      }
    }
    return {
      camera,
      setCamera,
      france,
      spain
    }
  },
  template: `
<button @click="setCamera(france)">France</button>
<button @click="setCamera(spain)">Spain</button>

<div style="height:95%">
  <m-map
    :animation="{ duration: 500 }"
    v-model:center="camera.center"
    v-model:zoom="camera.zoom"
    v-model:pitch="camera.pitch"
    v-model:bearing="camera.bearing"
    tiles="https://demotiles.maplibre.org/style.json" />
</div>`
})
export const Animation = TemplateAnimation.bind({})

const TemplateMethods = () => ({
  components: {
    MMap
  },
  setup () {
    // data
    const mapRef = ref(null)
    const bounds = ref(null)
    // methods
    const getBounds = lngLat => {
      if (mapRef.value) {
        bounds.value = mapRef.value.map.getBounds().toArray()
      }
    }
    return {
      mapRef,
      bounds,
      getBounds
    }
  },
  template: `
<button @click="getBounds">Get Bounds</button>

<m-map
  ref="mapRef"
  style="height:60%"
  tiles="https://demotiles.maplibre.org/style.json" />

<pre>{{ bounds }}</pre>`

})
export const Methods = TemplateMethods.bind({})

const TemplateEvents = () => ({
  components: {
    MMap
  },
  setup () {
    // data
    const moving = ref(false)
    // methods
    const setMoving = boolean => {
      moving.value = boolean
    }
    const click = e => {
      window.alert('Map clicked')
      console.log(e)
    }
    return {
      moving,
      click,
      setMoving
    }
  },
  template: `
<pre>moving: {{ moving }}</pre>
<m-map
  @click="click"
  @movestart="setMoving(true)"
  @moveend="setMoving(false)"
  style="height:95%"
  tiles="https://demotiles.maplibre.org/style.json" />`

})
export const Events = TemplateEvents.bind({})

const TemplateLang = () => ({
  components: {
    MMap
  },
  setup () {
    // data
    const center = ref([14,47])
    const lang = ref()
    // methods
    const setLang = l => {
      lang.value = l
    }
    return {
      center,
      lang,
      setLang
    }
  },
  template: `
<button @click="setLang('fr')">French</button>
<button @click="setLang('en')">English</button>
<button @click="setLang('es')">Spanish</button>
<button @click="setLang('it')">Italian</button>
<button @click="setLang('de')">German</button>
<button @click="setLang('zh')">Chinese</button>
<button @click="setLang('ru')">Russian</button>
<button @click="setLang('')">Multilanguage</button>

<m-map
  v-model:center="center"
  :lang="lang"
  :zoom="4"
  style="height:95%"
  tiles="https://api.maptiler.com/maps/streets/style.json?key=get_your_own_OpIi9ZULNHzrESv6T2vL" />`
})
export const Lang = TemplateLang.bind({})
