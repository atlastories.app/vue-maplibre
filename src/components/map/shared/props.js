import {
  optionsCorner,
  optionsPosition,
  optionsAlignment,
  validateNumber
} from '@/shared/options.js'

export const propsMap = {
  // custom vue map libre props
  animation: {
    type: Object
  },
  lang: {
    type: String
  },
  padding: {
    type: [Object, Number],
    default: 0
  },
  // replace 'style' to preserve standard inherited attribute
  tiles: {
    type: [Object, String]
  },
  // official map options
  // https://maplibre.org/maplibre-gl-js-docs/api/map/
  container: {
    type: [HTMLElement, String]
  },
  minZoom: {
    type: Number,
    default: 0,
    validator: n => validateNumber(n, 0, 24)
  },
  maxZoom: {
    type: Number,
    default: 22,
    validator: n => validateNumber(n, 0, 24)
  },
  minPitch: {
    type: Number,
    default: 0,
    validator: n => validateNumber(n, 0, 85)
  },
  maxPitch: {
    type: Number,
    default: 60,
    validator: n => validateNumber(n, 0, 85)
  },
  hash: {
    type: [Boolean, String]
  },
  interactive: {
    type: Boolean,
    default: true
  },
  bearingSnap: {
    type: Number,
    default: 7
  },
  pitchWithRotate: {
    type: Boolean,
    default: true
  },
  clickTolerance: {
    type: Number,
    default: 3
  },
  attributionControl: {
    type: Boolean,
    default: true
  },
  customAttribution: {
    type: [String, Array]
  },
  maplibreLogo: {
    type: Boolean
  },
  logoPosition: {
    type: String,
    default: optionsCorner[2],
    validator: s => optionsCorner.includes(s),
    options: optionsCorner
  },
  failIfMajorPerformanceCaveat: {
    type: Boolean
  },
  preserveDrawingBuffer: {
    type: Boolean
  },
  antialias: {
    type: Boolean
  },
  refreshExpiredTiles: {
    type: Boolean,
    default: true
  },
  maxBounds: {
    type: [Array, Object]
  },
  scrollZoom: {
    type: [Boolean, Object],
    default: true
  },
  boxZoom: {
    type: Boolean,
    default: true
  },
  dragRotate: {
    type: Boolean,
    default: true
  },
  dragPan: {
    type: [Boolean, Object],
    default: true
  },
  keyboard: {
    type: Boolean,
    default: true
  },
  doubleClickZoom: {
    type: Boolean,
    default: true
  },
  touchZoomRotate: {
    type: [Boolean, Object],
    default: true
  },
  touchPitch: {
    type: [Boolean, Object],
    default: true
  },
  cooperativeGestures: {
    type: [Boolean, Object]
  },
  trackResize: {
    type: Boolean,
    default: true
  },
  center: {
    type: [Array, Object],
    default: () => [0, 0]
  },
  zoom: {
    type: Number,
    default: 0
  },
  bearing: {
    type: Number,
    default: 0
  },
  pitch: {
    type: Number,
    default: 0
  },
  bounds: {
    type: Array,
  },
  fitBoundsOptions: {
    type: Object,
    default: () => {}
  },
  renderWorldCopies: {
    type: Boolean,
    default: true
  },
  maxCanvasSize: {
    type: Array
  },
  maxTileCacheSize: {
    type: Number
  },
  localIdeographFontFamily: {
    type: String,
    default: 'sans-serif'
  },
  transformCameraUpdate: {
    type: Function
  },
  transformRequest: {
    type: Function
  },
  collectResourceTiming: {
    type: Boolean
  },
  fadeDuration: {
    type: Number,
    default: 300
  },
  crossSourceCollisions: {
    type: Boolean,
    default: true
  },
  locale: {
    type: Object
  },
  pixelRatio: {
    type: Number
  }
}

export const propsMarker = {
  // custom vue map libre prop
  lngLat: {
    type: [Object, Array],
    required: true
  },
  className: {
    type: String
  },
  // standard marker options
  anchor: {
    type: String,
    default: optionsPosition[4],
    validator: s => optionsPosition.includes(s),
    options: optionsPosition
  },
  offset: {
    type: [Number, Array, Object]
  },
  color: {
    type: String,
    default: '#3FB1CE'
  },
  scale: {
    type: Number,
    default: 1
  },
  draggable: {
    type: Boolean
  },
  clickTolerance: {
    type: Number,
    default: 0
  },
  rotation: {
    type: Number,
    default: 0
  },
  pitchAlignment: {
    type: String,
    default: optionsAlignment[2],
    validator: s => optionsAlignment.includes(s),
    options: optionsAlignment
  },
  rotationAlignment: {
    type: String,
    default: optionsAlignment[2],
    validator: s => optionsAlignment.includes(s),
    options: optionsAlignment
  }
}

export const propsPopup = {
  lngLat: {
    type: [Object, Array]
  },
  closeButton: {
    type: Boolean,
    default: true
  },
  closeOnClick: {
    type: Boolean,
    default: true
  },
  closeOnMove: {
    type: Boolean
  },
  focusAfterOpen: {
    type: Boolean,
    default: true
  },
  anchor: {
    type: String,
    default: optionsPosition[4],
    validator: s => optionsPosition.includes(s),
    options: optionsPosition
  },
  offset: {
    type: [Number, Array, Object]
  },
  className: {
    type: String
  },
  maxWidth: {
    type: String,
    default: '240px'
  }
}
