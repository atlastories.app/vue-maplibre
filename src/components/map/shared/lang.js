
const get = 'get'
const concat = 'concat'
const non = 'non'
const name = 'name:'
const latin = 'latin'
const nameNonLatin = name + 'non' + latin

const createString = lang => `{${name}${lang}}`

const createArray = (field, lang) => {
  const separator = field[0] === concat ? field[2] : field[4]
  if (lang === latin) return [concat, [get, `${name}${latin}`], separator, [get, nameNonLatin]]
  return ['coalesce', [get, `${name}${lang}`], [get, `${name}${latin}`], [get, nameNonLatin], separator]
}

const textFieldValue = (field, lang) => {
  if (!lang) lang = latin
  const type = typeof field
  if (type === 'string' && field.includes(name)) return createString(lang)
  if (type === 'object' && field[1][1].includes(name)) return createArray(field, lang)
  return field
}

// map lang
export const setLang = (lang, map) => {
  const styles = map.value.getStyle()
  for (const layer of styles.layers) {
    if (layer.layout && layer.layout['text-field']) {
      const value = textFieldValue(layer.layout['text-field'], lang)
      map.value.setLayoutProperty(layer.id, 'text-field', value)
    }
  }
}
