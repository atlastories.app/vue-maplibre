import { watch } from 'vue'

const methodName = key => 'set' + key[0].toUpperCase() + key.substring(1)

// this method must be called after the object has been created
export const watchInstance = (instance, props) => {
  for (const key of Object.keys(props)) {
    const method = methodName(key)
    if (instance.value[method]) {
      watch(() => props[key], val => {
        instance.value[method](val)
      })
    }
  }
}
