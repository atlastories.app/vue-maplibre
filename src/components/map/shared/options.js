import { toRaw } from 'vue'

// returns props without falsy values
export const setOptions = props => {
  return Object.entries(toRaw(props))
    .reduce((acc, prop) => {
      if (prop[1] || prop[1] === false) acc[prop[0]] = prop[1]
      return acc
    }, {})
}
