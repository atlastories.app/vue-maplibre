export const eventsMapEmit = [
  'load',
  'click',
  'moveend',
  'change',
  'update:center',
  'update:zoom',
  'update:pitch',
  'update:bearing'
]

export const eventsMap = [
  'wheel',
  'resize',
  'remove',
  'touchmove',
  'movestart',
  'move',
  'dragstart',
  'drag',
  'dragend',
  'zoomstart',
  'zoom',
  'zoomend',
  'rotatestart',
  'rotate',
  'rotateend',
  'pitchstart',
  'pitch',
  'pitchend',
  'boxzoomstart',
  'boxzoomend',
  'boxzoomcancel',
  'webglcontextlost',
  'webglcontextrestored',
  'render',
  'idle',
  'error',
  'data',
  'styledata',
  'sourcedata',
  'dataloading',
  'styledataloading',
  'sourcedataloading',
  'styleimagemissing',
  'dataabort',
  'sourcedataabort',
  'mousedown',
  'mouseup',
  'mouseover',
  'mouseout',
  'mousemove',
  'dblclick',
  'contextmenu',
  'touchstart',
  'touchend',
  'touchcancel'
]

// events handled by emit
export const eventsMarkerEmit = [
  'mounted',
  'click'
]

// events handled by attributes
export const eventsMarker = [
  'drag',
  'dragend',
  'dragstart'
]

// events for story
export const argsEventsMarker = [
  ...eventsMarkerEmit,
  ...eventsMarker
]

// events handled by attributes
export const eventsPopup = [
  'open',
  'close'
]

// events handled by emit
export const eventsImageEmit = [
  'mounted',
  'load',
  'error'
]
