export const eventsLayer = [
  'mounted'
]

export const eventsLayerAttrs = [
  'mouseenter',
  'mouseleave',
  'mousedown',
  'mouseup',
  'mouseover',
  'mouseout',
  'mousemove',
  'click',
  'dblclick',
  'contextmenu',
  'touchstart',
  'touchend',
  'touchcancel'
]

export const argsEvents = [
  ...eventsLayer,
  ...eventsLayerAttrs
]
