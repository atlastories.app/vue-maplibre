import kebabCase from 'kebab-case'
import { inject, onBeforeUnmount } from 'vue'
import { watchLayer, nameMethod } from './watch.js'
import { nameProp } from './props.js'
import { eventsLayerAttrs } from './events.js'
import { defineId } from '@/shared/uid.js'
import { defineHandlers, removeHandlers } from '@/shared/events.js'

// from options definitions and props object,
// returns initial parameter for layer
const optionsLayer = (id, type, opts, props, source) => {
  const o = Object.entries(props).reduce((acc, prop) => {
    const key = prop[0]
    const val = prop[1]
    if (val || val === false) {
      const group = opts[key].group
      const name = group ? nameProp(type, key) : kebabCase(key)
      if (group) acc[group][name] = val
      else acc[name] = val
    }
    return acc
  }, { id, type, layout: {}, paint: {} })
  if (source) o.source = source
  return o
}

// return the string for the 'before' param
const optionsBefore = (props, map) => {
  let id
  if (props.before === 'symbol') {
    const layers = map.value.getStyle().layers
    for (const l of layers) {
      if (l.type === 'symbol' && l.layout['text-field']) {
        id = l.id
        break
      }
    }
  } else if (props.before === 'first') {
    const layers = map.value.getStyle().layers
    id = layers[1].id
  } else if (props.before) {
    id = props.before
  }
  return id
}

// setup layer
export const setupLayer = (type, opts, props, emit) => {
  // injects
  const map = inject('map')
  let source
  if (type !== 'background') {
    source = props.source ? props.source : inject('source')
    if (!source) throw new Error(type + ' layer must be a child of a <MSource...> component')
  }

  // set options
  const id = defineId(props)
  const before = optionsBefore(props, map)
  const options = optionsLayer(id, type, opts, props, source)

  // add layer to map
  map.value.addLayer(options, before)
  emit('mounted', map.value.getLayer(id))

  // watch props
  watchLayer(type, opts, id, props, map)

  // events
  const handlers = defineHandlers(eventsLayerAttrs, map, id)

  // clean up
  onBeforeUnmount(() => {
    removeHandlers(handlers, map, id)
    const layer = map.value.getLayer(id)
    if (layer) map.value.removeLayer(id)
  })

  return {
    id,
    map,
    source
  }
}

// set up special icon and text, childs of symbol layer
export const setupSymbol = (type, opts, props, emit) => {
  // inject
  const map = inject('map')
  const id = inject('symbol')

  // set props
  for (const key of Object.keys(props)) {
    const method = opts[key] && opts[key].group ? nameMethod(opts[key].group) : null
    const name = nameProp(type, key)
    const val = props[key]
    if (val && method) map.value[method](id, name, val)
  }

  const layer = map.value.getLayer(id)
  emit('load', layer)

  // watch props
  watchLayer(type, opts, id, props, map)
}
