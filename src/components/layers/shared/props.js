import kebabCase from 'kebab-case'
import {
  color,
  optionsPosition,
  optionsVisibility,
  optionsAnchor,
  optionsAlignment,
  optionsLineCap,
  optionsLineJoin,
  optionsRasterResampling,
  optionsSymbolOrder,
  optionsSymbolPlacement,
  optionsSymbolOverlap,
  optionsIconFit,
  optionsTextJustify,
  optionsTextCase,
  optionsTextDirection,
  validateNumber
} from '@/shared/options.js'

// groups
const paint = 'paint'
const layout = 'layout'

// prefix a prop, ex: defineProp('fill', 'outlineColor') => 'fill-outline-color'
const propsNoPrefix = ['visibility'] // no prefix for this props
export const nameProp = (type, prop) => propsNoPrefix.includes(prop) ? kebabCase(prop) : type + '-' + kebabCase(prop)

// suffix prop name with 'Transition'
const namePropTransition = prop => prop + 'Transition'

// add props with 'propTransition' to set specific css transition for a property
export const defineTransitions = opts => {
  return Object.entries(opts).reduce((acc, o) => {
    const key = o[0]
    const val = o[1]
    acc[key] = val
    if (val.transition) {
      const tKey = namePropTransition(key)
      const tVal = { type: Object }
      if (val.group) tVal.group = val.group
      acc[tKey] = tVal
    }
    return acc
  }, {})
}

// props for all layers
export const propsLayer = {
  id: {
    type: String
  },
  source: {
    type: String
  },
  sourceLayer: {
    type: String
  },
  before: {
    type: String
  },
  maxzoom: {
    type: Number
  },
  minzoom: {
    type: Number
  },
  filter: {
    type: [Array]
  },
  visibility: {
    type: String,
    default: optionsVisibility[0],
    validator: s => optionsVisibility.includes(s),
    group: layout,
    options: optionsVisibility
  }
}

// props for multiple layers
export const propsLayerColor = {
  color: {
    type: [String, Array],
    default: color.black,
    group: paint,
    transition: true
  },
  opacity: {
    type: [Number, Array],
    default: 1,
    validator: n => validateNumber(n, 0, 1),
    group: paint,
    transition: true
  }
}

export const propsLayerPattern = {
  pattern: {
    type: String,
    group: paint,
    transition: true
  }
}

export const propsLayerTranslate = {
  translate: {
    type: Array,
    default: () => [0,0],
    group: paint,
    transition: true
  },
  translateAnchor: {
    type: String,
    default: optionsAnchor[0],
    validator: s => optionsAnchor.includes(s),
    group: paint,
    options: optionsAnchor
  }
}

export const propsLayerSortKey = {
  sortKey: {
    type: [Number, Array],
    group: layout
  }
}

// props specific to circle
export const propsLayerCircle = {
  blur: {
    type: [Number, Array],
    default: 0,
    validator: n => validateNumber(n, 0),
    group: paint,
    transition: true
  },
  pitchAlignment: {
    type: String,
    default: optionsAnchor[1],
    validator: s => optionsAnchor.includes(s),
    group: paint,
    options: optionsAnchor
  },
  pitchScale: {
    type: String,
    default: optionsAnchor[0],
    validator: s => optionsAnchor.includes(s),
    group: paint,
    options: optionsAnchor
  },
  radius: {
    type: [Number, Array],
    default: 5,
    validator: n => validateNumber(n, 0),
    group: paint,
    transition: true
  },
  strokeColor: {
    type: [String, Array],
    default: color.black,
    group: paint,
    transition: true
  },
  strokeOpacity: {
    type: [Number, Array],
    default: 1,
    validator: n => validateNumber(n, 0, 1),
    group: paint,
    transition: true
  },
  strokeWidth: {
    type: [Number, Array],
    default: 0,
    validator: n => validateNumber(n, 0),
    group: paint,
    transition: true
  }
}

export const propsLayerFill = {
  antialias: {
    type: Boolean,
    default: true,
    group: paint
  },
  outlineColor: {
    type: [String, Array],
    group: paint,
    transition: true
  }
}

export const propsLayerFillExtrusion = {
  base: {
    type: [Number, Array],
    default: 0,
    validator: n => validateNumber(n, 0),
    group: paint,
    transition: true
  },
  height: {
    type: [Number, Array],
    default: 0,
    validator: n => validateNumber(n, 0),
    group: paint,
    transition: true
  },
  verticalGradient: {
    type: Boolean,
    default: true,
    group: paint
  }
}

export const propsLayerHeatmap = {
  color: {
    type: [String, Array],
    default: () => [
      'interpolate',
      ['linear'], ['heatmap-density'],
      0, 'rgba(0, 0, 255, 0)',
      0.1, 'royalblue',
      0.3, 'cyan',
      0.5, 'lime',
      0.7, 'yellow',
      1, 'red'
    ],
    group: paint
  },
  intensity: {
    type: [Number, Array],
    default: 1,
    validator: n => validateNumber(n, 0, 1),
    group: paint,
    transition: true
  },
  radius: {
    type: [Number, Array],
    default: 30,
    validator: n => validateNumber(n, 1),
    group: paint,
    transition: true
  },
  weight: {
    type: [Number, Array],
    default: 0.5,
    validator: n => validateNumber(n, 0, 1),
    group: paint
  }
}

export const propsLayerHillshade = {
  // custom prop, if true add the setTerrain
  terrain: {
    type: Boolean,
    default: true
  },
  before: {
    type: String,
    default: 'water'
  },
  accentColor: {
    type: [String, Array],
    default: color.black,
    group: paint,
    transition: true
  },
  exaggeration: {
    type: [Number, Array],
    default: 0.5,
    validator: n => validateNumber(n, 0, 1),
    group: paint,
    transition: true
  },
  highlightColor: {
    type: [String, Array],
    default: color.white,
    group: paint,
    transition: true
  },
  illuminationAnchor: {
    type: String,
    default: optionsAnchor[1],
    validator: s => optionsAnchor.includes(s),
    group: paint,
    options: optionsAnchor
  },
  illuminationDirection: {
    type: [Number, Array],
    default: 335,
    validator: n => validateNumber(n, 0, 359),
    group: paint
  },
  shadowColor: {
    type: [String, Array],
    default: color.black,
    group: paint,
    transition: true
  }
}

export const propsLayerLine = {
  blur: {
    type: [Number, Array],
    default: 0,
    validator: n => validateNumber(n, 0),
    group: paint,
    transition: true
  },
  cap: {
    type: String,
    default: optionsLineCap[0],
    validator: s => optionsLineCap.includes(s),
    group: layout,
    options: optionsLineCap
  },
  dasharray: {
    type: Array,
    group: paint,
    transition: true
  },
  gapWidth: {
    type: [Number, Array],
    default: 0,
    validator: n => validateNumber(n, 0),
    group: paint,
    transition: true
  },
  gradient: {
    type: [String, Array],
    group: paint
  },
  join: {
    type: String,
    default: optionsLineJoin[2],
    validator: s => optionsLineJoin.includes(s),
    group: layout,
    options: optionsLineJoin
  },
  miterLimit: {
    type: [Number, Array],
    default: 2,
    group: layout
  },
  offset: {
    type: [Number, Array],
    default: 1,
    group: paint,
    transition: true
  },
  roundLimit: {
    type: [Number, Array],
    default: 1.05,
    group: layout
  },
  width: {
    type: [Number, Array],
    default: 1,
    validator: n => validateNumber(n, 0),
    group: paint,
    transition: true
  }
}

export const propsLayerRaster = {
  brightnessMax: {
    type: [Number, Array],
    default: 1,
    validator: n => validateNumber(n, 0, 1),
    group: paint,
    transition: true
  },
  brightnessMin: {
    type: [Number, Array],
    default: 0,
    validator: n => validateNumber(n, 0, 1),
    group: paint,
    transition: true
  },
  contrast: {
    type: [Number, Array],
    default: 0,
    validator: n => validateNumber(n, -1, 1),
    group: paint,
    transition: true
  },
  fadeDuration: {
    type: [Number, Array],
    default: 300,
    validator: n => validateNumber(n, 0),
    group: paint
  },
  hueRotate: {
    type: [Number, Array],
    default: 0,
    group: paint,
    transition: true
  },
  opacity: {
    type: [Number, Array],
    default: 1,
    validator: n => validateNumber(n, 0, 1),
    group: paint
  },
  resampling: {
    type: String,
    default: optionsRasterResampling[0],
    validator: s => optionsRasterResampling.includes(s),
    group: paint,
    options: optionsRasterResampling
  },
  saturation: {
    type: [Number, Array],
    default: 0,
    validator: n => validateNumber(n, -1, 1),
    group: paint,
    transition: true
  }
}

export const propsLayerSymbol = {
  // propsLayerSortKey
  avoidEdges: {
    type: Boolean,
    group: layout
  },
  placement: {
    type: String,
    default: optionsSymbolPlacement[0],
    validator: s => optionsSymbolPlacement.includes(s),
    group: layout,
    options: optionsSymbolPlacement
  },
  spacing: {
    type: [Number, Array],
    default: 250,
    validator: n => validateNumber(n, 1),
    group: layout
  },
  zOrder: {
    type: String,
    default: optionsSymbolOrder[0],
    validator: s => optionsSymbolOrder.includes(s),
    group: layout,
    options: optionsSymbolOrder
  }
}

export const propsLayerSymbolChild = {
  allowOverlap: {
    type: Boolean,
    group: layout
  },
  anchor: {
    type: String,
    default: optionsPosition[0],
    validator: s => optionsPosition.includes(s),
    group: layout,
    options: optionsPosition
  },
  haloBlur: {
    type: [Number, Array],
    default: 0,
    validator: n => validateNumber(n, 0),
    group: paint,
    transition: true
  },
  haloColor: {
    type: [String, Array],
    default: 'rgba(0, 0, 0, 0)',
    group: paint,
    transition: true
  },
  haloWidth: {
    type: [Number, Array],
    default: 0,
    validator: n => validateNumber(n, 0),
    group: paint,
    transition: true
  },
  ignorePlacement: {
    type: Boolean,
    group: layout
  },
  offset: {
    type: Array,
    default: () => [0,0],
    group: layout
  },
  optional: {
    type: Boolean,
    group: layout
  },
  overlap: {
    type: String,
    validator: s => optionsSymbolOverlap.includes(s),
    group: layout,
    options: optionsSymbolOverlap
  },
  pitchAlignment: {
    type: String,
    default: optionsAlignment[2],
    validator: s => optionsAlignment.includes(s),
    group: layout,
    options: optionsAlignment
  },
  rotate: {
    type: [Number, Array],
    default: 0,
    group: layout
  },
  rotationAlignment: {
    type: String,
    default: optionsAlignment[2],
    validator: s => optionsAlignment.includes(s),
    group: layout,
    options: optionsAlignment
  }
}

export const propsLayerSymbolIcon = {
  // propsLayerColor
  // propsLayerTranslate
  // propsLayerSymbolChild
  image: {
    type: String,
    group: layout
  },
  keepUpright: {
    type: Boolean,
    group: layout
  },
  padding: {
    type: [Number, Object, Array],
    default: () => [2],
    group: layout
  },
  size: {
    type: [Number, Array],
    default: 1,
    validator: n => validateNumber(n, 0),
    group: layout
  },
  textFit: {
    type: String,
    default: optionsIconFit[0],
    validator: s => optionsIconFit.includes(s),
    group: layout,
    options: optionsIconFit
  },
  textFitPadding: {
    type: Array,
    default: () => [0,0,0,0],
    group: layout
  }
}

export const propsLayerSymbolText = {
  // propsLayerColor
  // propsLayerTranslate
  // propsLayerSymbolChild
  field: {
    type: [String, Array],
    default: '',
    group: layout
  },
  font: {
    type: Array,
    default: () => ['Open Sans Regular', 'Arial Unicode MS Regular'],
    group: layout
  },
  justify: {
    type: String,
    default: optionsTextJustify[2],
    validator: s => optionsTextJustify.includes(s),
    group: layout,
    options: optionsTextJustify
  },
  letterSpacing: {
    type: [Number, Array],
    default: 0,
    group: layout
  },
  lineHeight: {
    type: [Number, Array],
    default: 1.2,
    group: layout
  },
  maxAngle: {
    type: [Number, Array],
    default: 45,
    group: layout
  },
  maxWidth: {
    type: [Number, Array],
    default: 10,
    validator: n => validateNumber(n, 0),
    group: layout
  },
  padding: {
    type: [Number, Array],
    default: 2,
    group: layout
  },
  keepUpright: {
    type: Boolean,
    default: true,
    group: layout
  },
  radialOffset: {
    type: [Number, Array],
    default: 0,
    group: layout
  },
  size: {
    type: [Number, Array],
    default: 16,
    validator: n => validateNumber(n, 0),
    group: layout
  },
  transform: {
    type: String,
    default: optionsTextCase[0],
    validator: s => optionsTextCase.includes(s),
    group: layout,
    options: optionsTextCase
  },
  variableAnchor: {
    type: String,
    validator: s => optionsPosition.includes(s),
    group: layout,
    options: optionsPosition
  },
  writingMode: {
    type: String,
    validator: s => optionsTextDirection.includes(s),
    group: layout,
    options: optionsTextDirection
  }
}
