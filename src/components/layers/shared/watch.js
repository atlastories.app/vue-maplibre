import { watch } from 'vue'
import { nameProp } from './props.js'

// watch generic layer props
const watchLayerProps = (id, props, map) => {
  watch(() => props.before, val => {
    const hasLayer = map.value.getLayer(val)
    if (hasLayer) map.value.moveLayer(id, val)
  })
  watch([() => props.minzoom, () => props.maxzoom], ([minzoom, maxzoom]) => {
    map.value.setLayerZoomRange(id, minzoom, maxzoom)
  })
  watch(() => props.filter, val => {
    map.value.setFilter(id, val)
  })
  watch(() => props.visibility, val => {
    map.value.setLayoutProperty(id, 'visibility', val)
  })
}

// method name to update layer, depending on prop type
export const nameMethod = group => group === 'paint' ? 'setPaintProperty' : 'setLayoutProperty'

// watch layer props and trigger the method defined above
// ex: watching 'props.outlineColor' for 'fill' layer
// will trigger map.setPaintProperty(id, 'fill-outline-color')
const watchLayerPropsPrefixed = (type, opts, id, props, map) => {
  for (const prop of Object.entries(opts)) {
    if (prop[1].group) {
      const method = nameMethod(prop[1].group)
      const name = nameProp(type, prop[0])
      watch(() => props[name], val => {
        map.value[method](id, name, val)
      })
    }
  }
}

// watch all layer props
export const watchLayer = (type, opts, id, props, map) => {
  watchLayerProps(id, props, map)
  watchLayerPropsPrefixed(type, opts, id, props, map)
}
