import MMap from '@/components/map/MMap.vue'
import MSourceRasterDem from '@/components/sources/MSourceRasterDem.vue'
import MLayerHillshade, { opts } from '@/components/layers/MLayerHillshade.vue'
import { defineArgs } from '@/shared/args.js'
import { argsEvents } from '../shared/events.js'

export default {
  title: 'Layers/MLayerHillshade',
  component: MLayerHillshade,
  argTypes: defineArgs(opts, argsEvents)
}

const Template = args => ({
  components: {
    MMap,
    MSourceRasterDem,
    MLayerHillshade
  },
  setup () {
    return { args }
  },
  template: `
  <m-map
    tiles="https://api.maptiler.com/maps/streets/style.json?key=get_your_own_OpIi9ZULNHzrESv6T2vL"
    :center="[11.39085, 47.27574]"
    :zoom="12"
    :pitch="52"
    :tiles="style"
    :maxZoom="18"
    :maxPitch="85">

    <m-source-raster-dem
      url="https://demotiles.maplibre.org/terrain-tiles/tiles.json"
      :tileSize="256">

      <m-layer-hillshade
        v-bind="args" />

    </m-source-raster-dem>

  </m-map>`
})

export const Example = Template.bind({})
Example.args = {
  shadowColor: '#473B24',
  exaggeration: .2
}

export const Color = Template.bind({})
Color.args = {
  shadowColor: 'orange',
  exaggeration: .2
}
