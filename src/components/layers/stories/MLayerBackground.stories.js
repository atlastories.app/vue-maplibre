import MMap from '@/components/map/MMap.vue'
import MLayerBackground, { opts } from '@/components/layers/MLayerBackground.vue'
import { defineArgs } from '@/shared/args.js'
import { argsEvents } from '../shared/events.js'

export default {
  title: 'Layers/MLayerBackground',
  component: MLayerBackground,
  argTypes: defineArgs(opts, argsEvents)
}

const Template = args => ({
  components: {
    MMap,
    MLayerBackground
  },
  setup () {
    return {
      args
    }
  },
  template: `
  <m-map
    tiles="https://demotiles.maplibre.org/style.json">

    <m-layer-background
      v-bind="args" />

  </m-map>`
})

export const Example = Template.bind({})
Example.args = {
  color: '#666',
  opacity: .6
}
