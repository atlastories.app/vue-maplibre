import MMap from '@/components/map/MMap.vue'
import MSourceGeojson from '@/components/sources/MSourceGeojson.vue'
import MLayerCircle from '@/components/layers/MLayerCircle.vue'
import MLayerHeatmap, { opts } from '@/components/layers/MLayerHeatmap.vue'
import { defineArgs } from '@/shared/args.js'
import { argsEvents } from '../shared/events.js'

export default {
  title: 'Layers/MLayerHeatmap',
  component: MLayerHeatmap,
  argTypes: defineArgs(opts, argsEvents)
}

const Template = args => ({
  components: {
    MMap,
    MSourceGeojson,
    MLayerCircle,
    MLayerHeatmap
  },
  setup () {
    return { args }
  },
  template: `
  <m-map
    tiles="https://api.maptiler.com/maps/streets/style.json?key=get_your_own_OpIi9ZULNHzrESv6T2vL"
    :center="[-120, 50]"
    :zoom="2">
    <m-source-geojson
      data="https://atlastories.gitlab.io/vue-maplibre/earthquakes.geojson">

      <m-layer-heatmap
        v-bind="args" />

      <m-layer-circle
        :minzoom="7"
        :radius="[
          'interpolate',
          ['linear'], ['zoom'],
          7, ['interpolate', ['linear'], ['get', 'mag'], 1, 1, 6, 4],
          16, ['interpolate', ['linear'], ['get', 'mag'], 1, 5, 6, 50]
        ]"
        :color="[
          'interpolate',
          ['linear'], ['get', 'mag'],
          1, 'rgba(33,102,172,0)',
          2, 'rgb(103,169,207)',
          3, 'rgb(209,229,240)',
          4, 'rgb(253,219,199)',
          5, 'rgb(239,138,98)',
          6, 'rgb(178,24,43)'
        ]"
        strokeColor="white"
        :strokeWidth="1"
        :opacity="[
          'interpolate',
          ['linear'], ['zoom'],
          7, 0,
          8, 1
        ]" />

    </m-source-geojson>
  </m-map>`
})

export const Example = Template.bind({})
Example.args = {
  maxzoom: 9,
  weight: [
    'interpolate',
    ['linear'], ['get', 'mag'],
    0, 0,
    6, 1
  ],
  intensity: [
    'interpolate',
    ['linear'], ['zoom'],
    0, 1,
    9, 3
  ],
  color: [
    'interpolate',
    ['linear'], ['heatmap-density'],
    0, 'rgba(33,102,172,0)',
    0.2, 'rgb(103,169,207)',
    0.4, 'rgb(209,229,240)',
    0.6, 'rgb(253,219,199)',
    0.8, 'rgb(239,138,98)',
    1, 'rgb(178,24,43)'
  ],
  radius: [
    'interpolate',
    ['linear'], ['zoom'],
    0, 2,
    9, 20
  ],
  opacity: [
    'interpolate',
    ['linear'], ['zoom'],
    7, 1,
    9, 0
  ]
}
