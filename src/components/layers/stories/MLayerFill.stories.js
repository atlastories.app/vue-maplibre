import MMap from '@/components/map/MMap.vue'
import MSourceGeojson from '@/components/sources/MSourceGeojson.vue'
import MLayerFill, { opts } from '@/components/layers/MLayerFill.vue'
import { defineArgs } from '@/shared/args.js'
import { argsEvents } from '../shared/events.js'

export default {
  title: 'Layers/MLayerFill',
  component: MLayerFill,
  argTypes: defineArgs(opts, argsEvents)
}

const Template = args => ({
  components: {
    MMap,
    MSourceGeojson,
    MLayerFill
  },
  setup () {
    const geojson = {
      type: 'Feature',
      geometry: {
        type: 'Polygon',
        coordinates: [
          [
            [-67.13734351262877, 45.137451890638886],
            [-66.96466, 44.8097],
            [-68.03252, 44.3252],
            [-69.06, 43.98],
            [-70.11617, 43.68405],
            [-70.64573401557249, 43.090083319667144],
            [-70.75102474636725, 43.08003225358635],
            [-70.79761105007827, 43.21973948828747],
            [-70.98176001655037, 43.36789581966826],
            [-70.94416541205806, 43.46633942318431],
            [-71.08482, 45.3052400000002],
            [-70.6600225491012, 45.46022288673396],
            [-70.30495378282376, 45.914794623389355],
            [-70.00014034695016, 46.69317088478567],
            [-69.23708614772835, 47.44777598732787],
            [-68.90478084987546, 47.184794623394396],
            [-68.23430497910454, 47.35462921812177],
            [-67.79035274928509, 47.066248887716995],
            [-67.79141211614706, 45.702585354182816],
            [-67.13734351262877, 45.137451890638886]
          ]
        ]
      }
    }
    return {
      args,
      geojson
    }
  },
  template: `
  <m-map
    tiles="https://api.maptiler.com/maps/streets/style.json?key=get_your_own_OpIi9ZULNHzrESv6T2vL"
    :center="[-68.13734351262877, 45.137451890638886]"
    :zoom="5">
    <m-source-geojson
      :data="geojson">

      <m-layer-fill
        v-bind="args" />

    </m-source-geojson>
  </m-map>`
})

export const Example = Template.bind({})
Example.args = {
  color: '#088',
  opacity: .8
}
