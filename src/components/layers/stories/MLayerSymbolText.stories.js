import MMap from '@/components/map/MMap.vue'
import MImage from '@/components/map/MImage.vue'
import MSourceGeojson from '@/components/sources/MSourceGeojson.vue'
import MLayerSymbol from '@/components/layers/MLayerSymbol.vue'
import MLayerSymbolIcon from '@/components/layers/MLayerSymbolIcon.vue'
import MLayerSymbolText, { opts } from '@/components/layers/MLayerSymbolText.vue'
import { defineArgs } from '@/shared/args.js'
import { argsEvents } from '../shared/events.js'

export default {
  title: 'Layers/MLayerSymbolText',
  component: MLayerSymbolText,
  argTypes: defineArgs(opts, argsEvents)
}

const Template = args => ({
  components: {
    MMap,
    MImage,
    MSourceGeojson,
    MLayerSymbol,
    MLayerSymbolIcon,
    MLayerSymbolText
  },
  setup () {
    const img = 'https://atlastories.gitlab.io/vue-maplibre/osgeo-logo.png'
    const geojson = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          geometry: { type: 'Point', coordinates: [100.4933, 13.7551] },
          properties: { year: '2004' }
        },
        {
          type: 'Feature',
          geometry: { type: 'Point', coordinates: [6.6523, 46.5535] },
          properties: { year: '2006' }
        },
        {
          type: 'Feature',
          geometry: { type: 'Point', coordinates: [-123.3596, 48.4268] },
          properties: { year: '2007' }
        },
        {
          type: 'Feature',
          geometry: { type: 'Point', coordinates: [18.4264, -33.9224] },
          properties: { year: '2008' }
        },
        {
          type: 'Feature',
          geometry: { type: 'Point', coordinates: [151.195, -33.8552] },
          properties: { year: '2009' }
        },
        {
          type: 'Feature',
          geometry: { type: 'Point', coordinates: [2.1404, 41.3925] },
          properties: { year: '2010' }
        },
        {
          type: 'Feature',
          geometry: { type: 'Point', coordinates: [-104.8548, 39.7644] },
          properties: { year: '2011' }
        },
        {
          type: 'Feature',
          geometry: { type: 'Point', coordinates: [-1.1665, 52.9539] },
          properties: { year: '2013' }
        },
        {
          type: 'Feature',
          geometry: { type: 'Point', coordinates: [-122.6544, 45.5428] },
          properties: { year: '2014' }
        },
        {
          type: 'Feature',
          geometry: { type: 'Point', coordinates: [126.974, 37.5651] },
          properties: { year: '2015' }
        },
        {
          type: 'Feature',
          geometry: { type: 'Point', coordinates: [7.1112, 50.7255] },
          properties: { year: '2016' }
        },
        {
          type: 'Feature',
          geometry: { type: 'Point', coordinates: [-71.0314, 42.3539] },
          properties: { year: '2017' }
        },
        {
          type: 'Feature',
          geometry: { type: 'Point', coordinates: [39.2794, -6.8173] },
          properties: { year: '2018' }
        },
        {
          type: 'Feature',
          geometry: { type: 'Point', coordinates: [26.0961, 44.4379] },
          properties: { year: '2019' }
        },
        {
          type: 'Feature',
          geometry: { type: 'Point', coordinates: [-114.0879, 51.0279] },
          properties: { year: '2020' }
        }
      ]
    }
    return {
      args,
      img,
      geojson
    }
  },
  template: `
  <m-map
    tiles="https://api.maptiler.com/maps/positron/style.json?key=get_your_own_OpIi9ZULNHzrESv6T2vL">
    <m-image
      :src="img">
      <m-source-geojson
        :data="geojson">
        <m-layer-symbol>
          <m-layer-symbol-icon
            :image="img" />

          <m-layer-symbol-text
            v-bind="args" />

        </m-layer-symbol>
      </m-source-geojson>
    </m-image>
  </m-map>`
})

export const Example = Template.bind({})
Example.args = {
  field: ['get', 'year'],
  font: [
    'Open Sans Semibold',
    'Arial Unicode MS Bold'
  ],
  offset: [0, 1.25],
  anchor: 'top'
}
