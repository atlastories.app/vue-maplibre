import MMap from '@/components/map/MMap.vue'
import MSourceRaster from '@/components/sources/MSourceRaster.vue'
import MLayerRaster, { opts } from '@/components/layers/MLayerRaster.vue'
import { defineArgs } from '@/shared/args.js'
import { argsEvents } from '../shared/events.js'

export default {
  title: 'Layers/MLayerRaster',
  component: MLayerRaster,
  argTypes: defineArgs(opts, argsEvents)
}

const Template = args => ({
  components: {
    MMap,
    MSourceRaster,
    MLayerRaster
  },
  setup () {
    const attribution = 'Map tiles by <a target="_top" rel="noopener" href="http://stamen.com">Stamen Design</a>, under <a target="_top" rel="noopener" href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a>. Data by <a target="_top" rel="noopener" href="http://openstreetmap.org">OpenStreetMap</a>, under <a target="_top" rel="noopener" href="http://creativecommons.org/licenses/by-sa/3.0">CC BY SA</a>'
    return {
      attribution,
      args
    }
  },
  template: `
  <m-map
    tiles="https://api.maptiler.com/maps/streets/style.json?key=get_your_own_OpIi9ZULNHzrESv6T2vL"
    :center="[-74.5, 40]"
    :zoom="2"
    :minZoom="0"
    :maxZoom="22">
    <m-source-raster
      :tiles="[ 'https://stamen-tiles.a.ssl.fastly.net/watercolor/{z}/{x}/{y}.jpg' ]"
      :tileSize="256"
      :attribution="attribution">

      <m-layer-raster
        v-bind="args" />

    </m-source-raster>
  </m-map>`
})

export const Example = Template.bind({})
Example.args = {
  minzoom: 0,
  maxzoom: 22
}
