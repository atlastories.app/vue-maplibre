import MMap from '@/components/map/MMap.vue'
import MSourceGeojson from '@/components/sources/MSourceGeojson.vue'
import MLayerFillExtrusion, { opts } from '@/components/layers/MLayerFillExtrusion.vue'
import { defineArgs } from '@/shared/args.js'
import { argsEvents } from '../shared/events.js'

export default {
  title: 'Layers/MLayerFillExtrusion',
  component: MLayerFillExtrusion,
  argTypes: defineArgs(opts, argsEvents)
}

const TemplateBuildings = args => ({
  components: {
    MMap,
    MLayerFillExtrusion
  },
  setup () {
    return { args }
  },
  template: `
  <m-map
    antialias
    tiles="https://api.maptiler.com/maps/streets/style.json?key=get_your_own_OpIi9ZULNHzrESv6T2vL"
    :center="[-74.0066, 40.7135]"
    :zoom="15.5"
    :pitch="45"
    :bearing="-17.6">

    <m-layer-fill-extrusion
      v-bind="args" />

  </m-map>`
})

export const Buildings = TemplateBuildings.bind({})
Buildings.args = {
  before: 'symbol',
  source: 'openmaptiles',
  sourceLayer: 'building',
  filter: ['==', 'extrude', 'true'],
  minzoom: 15,
  color: '#aaa',
  // use an 'interpolate' expression to add a smooth transition effect to the
  // buildings as the user zooms in
  height: [
    'interpolate', ['linear'], ['zoom'],
    15, 0, 15.05,
    ['get', 'height']
  ],
  base: [
    'interpolate', ['linear'], ['zoom'],
    15, 0, 15.05,
    ['get', 'min_height']
  ],
  opacity: 0.6
}

const TemplateFloorPlan = args => ({
  components: {
    MMap,
    MSourceGeojson,
    MLayerFillExtrusion
  },
  setup () {
    return { args }
  },
  template: `
  <m-map
    antialias
    :tiles="{
      id: 'raster',
      version: 8,
      name: 'Raster tiles',
      center: [0, 0],
      zoom: 0,
      sources: {
        'raster-tiles': {
          type: 'raster',
          tiles: [
            'https://tile.openstreetmap.org/{z}/{x}/{y}.png',
          ],
          tileSize: 256,
          minzoom: 0,
          maxzoom: 19
        }
      },
      layers: [{
        id: 'background',
        type: 'background',
        paint: { 'background-color': '#e0dfdf' }
      }, {
        id: 'simple-tiles',
        type: 'raster',
        source: 'raster-tiles'
      }]
    }"
    :center="[-87.61694, 41.86625]"
    :zoom="16"
    :pitch="40"
    :bearing="20">
    <m-source-geojson
      data="https://atlastories.gitlab.io/vue-maplibre/indoor-3d-map.geojson">

      <m-layer-fill-extrusion
        v-bind="args" />

    </m-source-geojson>
  </m-map>`
})

export const FloorPlan = TemplateFloorPlan.bind({})
FloorPlan.args = {
  color: ['get', 'color'],
  height: ['get', 'height'],
  base: ['get', 'base_height'],
  opacity: 0.5
}
