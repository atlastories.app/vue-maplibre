import MMap from '@/components/map/MMap.vue'
import MControlTerrain, { opts } from '@/components/controls/MControlTerrain.vue'
import { defineArgs } from '@/shared/args.js'
import { eventsControl } from '../shared/events.js'

export default {
  title: 'Controls/MControlTerrain',
  component: MControlTerrain,
  argTypes: defineArgs(opts, eventsControl)
}

const Template = args => ({
  components: {
    MMap,
    MControlTerrain
  },
  setup () {
    const style = {
      version: 8,
      sources: {
        osm: {
          type: 'raster',
          tiles: ['https://a.tile.openstreetmap.org/{z}/{x}/{y}.png'],
          tileSize: 256,
          attribution: '&copy; OpenStreetMap Contributors',
          maxzoom: 19
        },
        terrainSource: {
          type: 'raster-dem',
          url: 'https://demotiles.maplibre.org/terrain-tiles/tiles.json',
          tileSize: 256
        },
        hillshadeSource: {
          type: 'raster-dem',
          url: 'https://demotiles.maplibre.org/terrain-tiles/tiles.json',
          tileSize: 256
        }
      },
      layers: [
        {
          id: 'osm',
          type: 'raster',
          source: 'osm'
        },
        {
          id: 'hills',
          type: 'hillshade',
          source: 'hillshadeSource',
          layout: { visibility: 'visible' },
          paint: { 'hillshade-shadow-color': '#473B24' }
        }
      ],
      terrain: {
        source: 'terrainSource',
        exaggeration: 1
      }
    }
    return {
      args,
      style
    }
  },
  template: `
  <m-map
    :tiles="style"
    :center="[11.39085, 47.27574]"
    :zoom="12"
    :pitch="52"
    :maxZoom="18"
    :maxPitch="85">

    <m-control-terrain
      v-bind="args" />

  </m-map>`
})

export const Default = Template.bind({})
Default.args = {
  source: 'terrainSource',
  exaggeration: 1
}

export const Position = Template.bind({})
Position.args = {
  source: 'terrainSource',
  position: 'bottom-right'
}
