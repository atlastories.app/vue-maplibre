import MMap from '@/components/map/MMap.vue'
import MControlNavigation, { opts } from '@/components/controls/MControlNavigation.vue'
import { defineArgs } from '@/shared/args.js'
import { eventsControl } from '../shared/events.js'

export default {
  title: 'Controls/MControlNavigation',
  component: MControlNavigation,
  argTypes: defineArgs(opts, eventsControl)
}

const Template = args => ({
  components: {
    MMap,
    MControlNavigation
  },
  setup () {
    return {
      args
    }
  },
  template: `
  <m-map
    tiles="https://demotiles.maplibre.org/style.json"
    :center="[11.39085, 47.27574]"
    :zoom="5"
    :pitch="50"
    :bearing="50">

    <m-control-navigation
      v-bind="args" />

  </m-map>`
})

export const Default = Template.bind({})
Default.args = {}

export const Position = Template.bind({})
Position.args = {
  position: 'bottom-left'
}

export const Zoom = Template.bind({})
Zoom.args = {
  showCompass: false
}

export const Compass = Template.bind({})
Compass.args = {
  showZoom: false
}

export const Pitch = Template.bind({})
Pitch.args = {
  visualizePitch: true
}
