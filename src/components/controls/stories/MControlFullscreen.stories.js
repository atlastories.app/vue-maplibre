import MMap from '@/components/map/MMap.vue'
import MControlFullscreen, { opts } from '@/components/controls/MControlFullscreen.vue'
import { defineArgs } from '@/shared/args.js'
import { eventsControl } from '../shared/events.js'

export default {
  title: 'Controls/MControlFullscreen',
  component: MControlFullscreen,
  argTypes: defineArgs(opts, eventsControl)
}

const Template = args => ({
  components: {
    MMap,
    MControlFullscreen
  },
  setup () {
    return {
      args
    }
  },
  template: `
  <m-map
    tiles="https://demotiles.maplibre.org/style.json">

    <m-control-fullscreen
      v-bind="args" />

  </m-map>`
})

export const Default = Template.bind({})
Default.args = {}

export const Position = Template.bind({})
Position.args = {
  position: 'bottom-left'
}
