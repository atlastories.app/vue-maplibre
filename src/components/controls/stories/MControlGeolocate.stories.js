import MMap from '@/components/map/MMap.vue'
import MControlGeolocate, { opts } from '@/components/controls/MControlGeolocate.vue'
import { defineArgs } from '@/shared/args.js'
import { eventsControl, eventsGeolocate } from '../shared/events.js'

const events = [...eventsControl, ...eventsGeolocate]

export default {
  title: 'Controls/MControlGeolocate',
  component: MControlGeolocate,
  argTypes: defineArgs(opts, events)
}

const Template = args => ({
  components: {
    MMap,
    MControlGeolocate
  },
  setup () {
    return {
      args
    }
  },
  template: `
  <m-map
    tiles="https://demotiles.maplibre.org/style.json">

    <m-control-geolocate
      v-bind="args" />

  </m-map>`
})

export const Default = Template.bind({})
Default.args = {}

export const Position = Template.bind({})
Position.args = {
  position: 'bottom-left'
}

export const TrackUserLocation = Template.bind({})
TrackUserLocation.args = {
  trackUserLocation: true
}
