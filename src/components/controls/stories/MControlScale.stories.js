import MMap from '@/components/map/MMap.vue'
import MControlScale, { opts } from '@/components/controls/MControlScale.vue'
import { defineArgs } from '@/shared/args.js'
import { eventsControl } from '../shared/events.js'

export default {
  title: 'Controls/MControlScale',
  component: MControlScale,
  argTypes: defineArgs(opts, eventsControl)
}

const Template = args => ({
  components: {
    MMap,
    MControlScale
  },
  setup () {
    return {
      args
    }
  },
  template: `
  <m-map
    tiles="https://demotiles.maplibre.org/style.json">

    <m-control-scale
      v-bind="args" />

  </m-map>`
})

export const Default = Template.bind({})
Default.args = {}

export const Position = Template.bind({})
Position.args = {
  position: 'bottom-left'
}

export const Imperial = Template.bind({})
Imperial.args = {
  unit: 'imperial'
}
