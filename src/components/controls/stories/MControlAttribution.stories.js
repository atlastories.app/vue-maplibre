import MMap from '@/components/map/MMap.vue'
import MControlAttribution, { opts } from '@/components/controls/MControlAttribution.vue'
import { defineArgs } from '@/shared/args.js'
import { eventsControl } from '../shared/events.js'

export default {
  title: 'Controls/MControlAttribution',
  component: MControlAttribution,
  argTypes: defineArgs(opts, eventsControl)
}

const Template = args => ({
  components: {
    MMap,
    MControlAttribution
  },
  setup () {
    return {
      args
    }
  },
  template: `
  <m-map
    tiles="https://demotiles.maplibre.org/style.json">
    <m-control-attribution
      v-bind="args" />
  </m-map>`
})

export const Default = Template.bind({})
Default.args = {
  position: 'bottom-right',
  customAttribution: 'Lorem ipsum dolor sit amet.'
}

export const Compact = Template.bind({})
Compact.args = {
  position: 'bottom-right',
  customAttribution: 'Lorem ipsum dolor sit amet.',
  compact: true
}

export const Position = Template.bind({})
Position.args = {
  position: 'bottom-left',
  customAttribution: 'Lorem ipsum dolor sit amet.'
}
