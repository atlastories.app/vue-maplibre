import MMap from '@/components/map/MMap.vue'
import MSourceRasterDem from '@/components/sources/MSourceRasterDem.vue'
import MControlAttribution from '@/components/controls/MControlAttribution.vue'
import MControlFullscreen from '@/components/controls/MControlFullscreen.vue'
import MControlGeolocate from '@/components/controls/MControlGeolocate.vue'
import MControlTerrain from '@/components/controls/MControlTerrain.vue'
import MControlScale from '@/components/controls/MControlScale.vue'
import MControlNavigation from '@/components/controls/MControlNavigation.vue'
import MControlCustom, { opts } from '@/components/controls/MControlCustom.vue'
import { defineArgs } from '@/shared/args.js'
import { eventsControl } from '../shared/events.js'

export default {
  title: 'Controls/MControlCustom',
  component: MControlCustom,
  argTypes: defineArgs(opts, eventsControl)
}

const Template = args => ({
  components: {
    MMap,
    MControlCustom
  },
  setup () {
    const alert = () => window.alert('click')
    return {
      args,
      alert
    }
  },
  template: `
  <MMap
    tiles="https://demotiles.maplibre.org/style.json">

    <MControlCustom
      v-bind="args">
      <button
        @click="alert">
        Test
      </button>
    </MControlCustom>

  </MMap>`
})

export const Default = Template.bind({})
Default.args = {}

export const Position = Template.bind({})
Position.args = {
  position: 'bottom-left'
}

export const Demo = () => ({
  components: {
    MMap,
    MSourceRasterDem,
    MControlAttribution,
    MControlFullscreen,
    MControlGeolocate,
    MControlTerrain,
    MControlScale,
    MControlNavigation
  },
  template: `
  <m-map
    tiles="https://api.maptiler.com/maps/streets/style.json?key=get_your_own_OpIi9ZULNHzrESv6T2vL"
    :center="[11.39085, 47.27574]"
    :zoom="12"
    :pitch="52"
    :maxZoom="18"
    :maxPitch="85">

    <m-source-raster-dem
      id="hills"
      url="https://demotiles.maplibre.org/terrain-tiles/tiles.json"
      :tileSize="256" />

    <m-control-attribution
      :compact="true"
      position="top-left" />

    <m-control-navigation />
    <m-control-geolocate />
    <m-control-terrain
      source="hills" />

    <m-control-fullscreen
      position="bottom-right" />

    <m-control-scale
      position="bottom-left" />

  </m-map>`
})

const TemplateCustom = () => ({
  components: {
    MMap,
    MControlCustom,
    MControlNavigation
  },
  setup () {
    const click = () => {
      alert('Custom control clicked')
    }
    return {
      click
    }
  },
  template: `
  <m-map
    tiles="https://demotiles.maplibre.org/style.json">

    <m-control-navigation />
    <m-control-custom>
      <button
        @click="click">
        Test
      </button>
    </m-control-custom>

  </m-map>`
})

export const Custom = TemplateCustom.bind({})
