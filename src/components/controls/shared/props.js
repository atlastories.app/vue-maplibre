import {
  optionsCorner,
  optionsControlScale
} from '@/shared/options.js'

export const propsControl = {
  position: {
    type: String,
    default: optionsCorner[1],
    validator: s => optionsCorner.includes(s),
    options: optionsCorner
  }
}

export const propsControlNavigation = {
  showCompass: {
    type: Boolean,
    default: true
  },
  showZoom: {
    type: Boolean,
    default: true
  },
  visualizePitch: {
    type: Boolean
  }
}

export const propsControlGeolocate = {
  positionOptions: {
    type: Object,
    default: () => ({ enableHighAccuracy: false, timeout:6000 })
  },
  fitBoundsOptions: {
    type: Object,
    default: () => ({ maxZoom: 15 })
  },
  trackUserLocation: {
    type: Boolean
  },
  showAccuracyCircle: {
    type: Boolean,
    default: true
  },
  showUserLocation: {
    type: Boolean,
    default: true
  }
}

export const propsControlAttribution = {
  compact: {
    type: Boolean
  },
  customAttribution: {
    type: [String, Array]
  }
}

export const propsControlScale = {
  maxWidth: {
    type: Number,
    default: 100
  },
  unit: {
    type: String,
    default: optionsControlScale[1],
    validator: s => optionsControlScale.includes(s),
    options: optionsControlScale
  }
}

export const propsControlFullscreen = {
  container: {
    type: HTMLElement
  }
}

export const propsControlTerrain = {
  source: {
    type: String,
    required: true
  },
  exaggeration: {
    type: Number,
    default: 1
  }
}
