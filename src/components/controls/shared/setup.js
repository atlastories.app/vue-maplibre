import { inject, ref, computed, watch, onBeforeUnmount } from 'vue'
import { defineHandlers, removeHandlers } from '@/shared/events.js'

// remove falsy values and prop position
export const optionsControl = props => {
  return Object.entries(props).reduce((acc, prop) => {
    const key = prop[0]
    const val = prop[1]
    if (key !== 'position') acc[key] = val
    return acc
  }, {})
}

// instance is a control constructor
// events are the events related to this instance
export const setupControl = (Instance, props, emit, events = []) => {
  // inject
  const map = inject('map')

  // computed options
  const options = computed(() => {
    return optionsControl(props)
  })

  // data
  const control = ref(new Instance(options.value))

  // add to map
  map.value.addControl(control.value, props.position)
  emit('mounted', control.value)

  // watch and reset
  const reset = () => {
    map.value.removeControl(control.value)
    control.value = new Instance(options.value)
    map.value.addControl(control.value, props.position)
    emit('updated', control.value)
  }
  watch(() => props.position, reset)
  watch(options, reset)

  // events
  const handlers = defineHandlers(events, control)

  // clean up
  onBeforeUnmount(() => {
    removeHandlers(handlers, control)
    map.value.removeControl(control.value)
  })

  return {
    map,
    control
  }
}
