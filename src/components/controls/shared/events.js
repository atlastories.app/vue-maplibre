export const eventsControl = [
  'mounted',
  'updated'
]

export const eventsFullscreen = [
  'fullscreenstart',
  'fullscreenend'
]

export const eventsGeolocate = [
  'error',
  'geolocate',
  'outofmaxbounds',
  'trackuserlocationend',
  'trackuserlocationstart'
]
