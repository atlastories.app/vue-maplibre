import { getCurrentInstance, toRaw } from 'vue'

// returns the prop id or a unique id
export const defineId = props => props && props.id ? props.id : 'i' + getCurrentInstance().uid
