// storybook arg types
export const args = {
  boolean: 'boolean',
  number: 'number',
  range: 'range',
  object: 'object',
  file: 'file',
  radio: 'radio',
  inlineRadio: 'inline-radio',
  check: 'check',
  inlineCheck: 'inline-check',
  select: 'select',
  multiSelect: 'multi-select',
  text: 'text',
  color: 'color',
  date: 'date'
}

// number field
export const argNumber = (min, max) => {
  const type = args.number
  const a = { type }
  if (min) a.min = min
  if (max) {
    a.max = max
    a.step = max === 1 ? .01 : 1
    a.type = args.range
  }
  return a
}

// color field
export const argColor = { type: args.color }

// return a string: click => onClick
export const argEvent = name => 'on' + name[0].toUpperCase() + name.substring(1)

// from an array of event names, return storybook args for those events
export const argEvents = events => {
  return events.reduce((acc, e) => {
    const name = argEvent(e)
    acc[name] = { action: e, table: { disable: true } }
    return acc
  }, {})
}

// from prop, extract control, options and defaultValue based on value
export const argOptions = opts => {
  return Object.entries(opts).reduce((acc, e) => {
    const key = e[0]
    const val = e[1]

    let control
    // if options control is a select
    if (val.options) control = val.options.length < 5 ? args.inlineRadio : args.select
    // if multiple types control is an object
    if (Array.isArray(val.type)) control = args.object
    // if one type
    if (val.type === String && !val.options) control = args.text
    if (val.type === Number) control = args.number
    if (val.type === Object) control = args.object
    if (val.type === Array) control = args.object
    if (val.type === Boolean) control = args.boolean

    if (control) {
      const c = { control }
      if (val.options) c.options = val.options
      if (val.default) {
        const table = {}
        if (typeof val.default === 'function') table.defaultValue = val.default()
        else table.defaultValue = val.default
        c.table = table
      }
      acc[key] = c
    }
    return acc
  }, {})
}

export const defineArgs = (opts, events = []) => {
  // const argsFromOptions = argOptions(opts)
  // const argsFromEvents = argEvents(events)
  // return Object.assign(argsFromOptions, argsFromEvents)
  return argOptions(opts)
}
