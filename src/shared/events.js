import { useAttrs } from 'vue'

// attribute name
export const attrEvent = name => 'on' + name[0].toUpperCase() + name.substring(1)

// generic event listeners based on an array of events and attributes
// returns an object with key as the event name, and value with handler
export const defineHandlers = (events, instance, layerid) => {
  // get attributes
  const attrs = useAttrs()

  // store handlers for future 'off'
  const handlers = {}
  for (const e of events) {
    // attribute name
    const key = attrEvent(e)
    // use only event attributes
    if (attrs[key]) {
      handlers[e] = attrs[key]
      // add event listener to map, instance or layer
      if (layerid) instance.value.on(e, layerid, handlers[e])
      else instance.value.on(e, handlers[e])
    }
  }
  return handlers
}

export const removeHandlers = (handlers, instance, layerid) => {
  for (const e of Object.keys(handlers)) {
    if (layerid) instance.value.off(e, layerid, handlers[e])
    else instance.value.off(e, handlers[e])
  }
}

/*
map events
---
wheel
resize
remove
touchmove
movestart
move
moveend
dragstart
drag
dragend
zoomstart
zoom
zoomend
rotatestart
rotate
rotateend
pitchstart
pitch
pitchend
boxzoomstart
boxzoomend
boxzoomcancel
webglcontextlost
webglcontextrestored
load
render
idle
error
data
styledata
sourcedata
dataloading
styledataloading
sourcedataloading
styleimagemissing
dataabort
sourcedataabort

map and layer event
---
mousedown
mouseup
mouseover
mouseout
mousemove
click
dblclick
contextmenu
touchstart
touchend
touchcancel

layer events
---
mouseenter
mouseleave

marker events
---
drag
dragend
dragstart

popup events
---
open
close
*/
