// available options for string properties
// and number validator

export const validateNumber = (n, min, max) => {
  let valid = true
  if (min && n < min) valid = false
  if (max && n > max) valid = false
  return valid
}

// common values

export const color = {
  black: '#000000',
  white: '#FFFFFF'
}

const common = {
  none: 'none',
  visible: 'visible',
  auto: 'auto',
  map: 'map',
  viewport: 'viewport'
}

export const optionsVisibility = [
  common.visible,
  common.none
]

export const optionsAnchor = [
  common.map,
  common.viewport
]

export const optionsAlignment = [
  common.map,
  common.viewport,
  common.auto
]

// position

const position = {
  center: 'center',
  left: 'left',
  right: 'right',
  top: 'top',
  bottom: 'bottom',
  topLeft: 'top-left',
  topRight: 'top-right',
  bottomLeft: 'bottom-left',
  bottomRight: 'bottom-right'
}

export const optionsCorner = [
  position.topLeft,
  position.topRight,
  position.bottomLeft,
  position.bottomRight
]

export const optionsPosition = [
  position.center,
  position.left,
  position.right,
  position.top,
  position.bottom,
  ...optionsCorner
]

// line

const line = {
  butt: 'butt',
  round: 'round',
  square: 'square',
  bevel: 'bevel',
  miter: 'miter'
}

export const optionsLineCap = [
  line.butt,
  line.round,
  line.square
]

export const optionsLineJoin = [
  line.bevel,
  line.round,
  line.miter
]

// raster

const raster = {
  linear: 'linear',
  nearest: 'nearest'
}

export const optionsRasterResampling = [
  raster.linear,
  raster.nearest
]

// symbol

const symbol = {
  never: 'never',
  always: 'always',
  cooperative: 'cooperative',
  point: 'point',
  line: 'line',
  lineCenter: 'line-center',
  viewportY: 'viewport-y',
  source: 'source'
}

export const optionsSymbolOverlap = [
  symbol.never,
  symbol.always,
  symbol.cooperative
]

export const optionsSymbolPlacement = [
  symbol.point,
  symbol.line,
  symbol.lineCenter
]

export const optionsSymbolOrder = [
  common.auto,
  symbol.viewportY,
  symbol.source
]

// symbol icon

const icon = {
  width: 'width',
  both: 'both'
}

export const optionsIconFit = [
  common.none,
  icon.width,
  icon.both
]

// symbol text

const text = {
  uppercase: 'uppercase',
  lowercase: 'lowercase',
  horizontal: 'horizontal',
  vertical: 'vertical'
}

export const optionsTextJustify = [
  common.auto,
  position.left,
  position.center,
  position.right
]

export const optionsTextCase = [
  common.none,
  text.uppercase,
  text.lowercase
]

export const optionsTextDirection = [
  text.horizontal,
  text.vertical
]

// sources

const sources = {
  xyz: 'xyz',
  tms: 'tms',
  terrarium: 'terrarium',
  mapbox: 'mapbox'
}

export const optionsSourceScheme = [
  sources.xyz,
  sources.tms
]

export const optionsSourceEnding = [
  sources.terrarium,
  sources.mapbox
]

// controls

const controls = {
  imperial: 'imperial',
  metric: 'metric',
  nautical: 'nautical'
}

export const optionsControlScale = [
  controls.imperial,
  controls.metric,
  controls.nautical
]
