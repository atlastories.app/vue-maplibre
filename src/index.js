// map
import MMap from '@/components/map/MMap.vue'
import MMarker from '@/components/map/MMarker.vue'
import MPopup from '@/components/map/MPopup.vue'
import MImage from '@/components/map/MImage.vue'

// sources
import MSourceGeojson from '@/components/sources/MSourceGeojson.vue'
import MSourceImage from '@/components/sources/MSourceImage.vue'
import MSourceRaster from '@/components/sources/MSourceRaster.vue'
import MSourceRasterDem from '@/components/sources/MSourceRasterDem.vue'
import MSourceVector from '@/components/sources/MSourceVector.vue'
import MSourceVideo from '@/components/sources/MSourceVideo.vue'

// layers
import MLayerBackground from '@/components/layers/MLayerBackground.vue'
import MLayerCircle from '@/components/layers/MLayerCircle.vue'
import MLayerFill from '@/components/layers/MLayerFill.vue'
import MLayerFillExtrusion from '@/components/layers/MLayerFillExtrusion.vue'
import MLayerHeatmap from '@/components/layers/MLayerHeatmap.vue'
import MLayerHillshade from '@/components/layers/MLayerHillshade.vue'
import MLayerLine from '@/components/layers/MLayerLine.vue'
import MLayerRaster from '@/components/layers/MLayerRaster.vue'
import MLayerSymbol from '@/components/layers/MLayerSymbol.vue'
import MLayerSymbolIcon from '@/components/layers/MLayerSymbolIcon.vue'
import MLayerSymbolText from '@/components/layers/MLayerSymbolText.vue'

// controls
import MControlAttribution from '@/components/controls/MControlAttribution.vue'
import MControlCustom from '@/components/controls/MControlCustom.vue'
import MControlFullscreen from '@/components/controls/MControlFullscreen.vue'
import MControlGeolocate from '@/components/controls/MControlGeolocate.vue'
import MControlNavigation from '@/components/controls/MControlNavigation.vue'
import MControlScale from '@/components/controls/MControlScale.vue'
import MControlTerrain from '@/components/controls/MControlTerrain.vue'

export {
  // map
  MMap,
  MMarker,
  MPopup,
  MImage,
  // sources
  MSourceGeojson,
  MSourceImage,
  MSourceRaster,
  MSourceRasterDem,
  MSourceVector,
  MSourceVideo,
  // layers
  MLayerBackground,
  MLayerCircle,
  MLayerFill,
  MLayerFillExtrusion,
  MLayerHeatmap,
  MLayerHillshade,
  MLayerLine,
  MLayerRaster,
  MLayerSymbol,
  MLayerSymbolIcon,
  MLayerSymbolText,
  // controls
  MControlAttribution,
  MControlCustom,
  MControlFullscreen,
  MControlGeolocate,
  MControlNavigation,
  MControlScale,
  MControlTerrain
}

export function install (app) {
  app.component('MMap', MMap)
  app.component('MMarker', MMarker)
  app.component('MPopup', MPopup)
  app.component('MImage', MImage)
  // sources
  app.component('MSourceGeojson', MSourceGeojson)
  app.component('MSourceImage', MSourceImage)
  app.component('MSourceRaster', MSourceRaster)
  app.component('MSourceRasterDem', MSourceRasterDem)
  app.component('MSourceVector', MSourceVector)
  app.component('MSourceVideo', MSourceVideo)
  // layers
  app.component('MLayerBackground', MLayerBackground)
  app.component('MLayerCircle', MLayerCircle)
  app.component('MLayerFill', MLayerFill)
  app.component('MLayerFillExtrusion', MLayerFillExtrusion)
  app.component('MLayerHeatmap', MLayerHeatmap)
  app.component('MLayerHillshade', MLayerHillshade)
  app.component('MLayerLine', MLayerLine)
  app.component('MLayerRaster', MLayerRaster)
  app.component('MLayerSymbol', MLayerSymbol)
  app.component('MLayerSymbolIcon', MLayerSymbolIcon)
  app.component('MLayerSymbolText', MLayerSymbolText)
  // controls
  app.component('MControlAttribution', MControlAttribution)
  app.component('MControlCustom', MControlCustom)
  app.component('MControlFullscreen', MControlFullscreen)
  app.component('MControlGeolocate', MControlGeolocate)
  app.component('MControlNavigation', MControlNavigation)
  app.component('MControlScale', MControlScale)
  app.component('MControlTerrain', MControlTerrain)
}

// plugin
const plugin = {
  version: APP_VERSION,
  install
}

export default plugin

// auto install in browser
let GlobalVue = null
if (typeof window !== 'undefined') {
  GlobalVue = window.Vue
} else if (typeof global !== 'undefined') {
  GlobalVue = global.Vue
}
if (GlobalVue) {
  GlobalVue.use(plugin)
}
