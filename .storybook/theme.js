import { create } from '@storybook/theming'

export default create({
  base: 'light',
  brandTitle: 'Vue MapLibre',
  brandUrl: 'https://atlastories.app.gitlab.io/vue-maplibre',
  brandImage: 'https://maplibre.org/img/maplibre-logo-dark.svg',
  brandTarget: '_self'
})
