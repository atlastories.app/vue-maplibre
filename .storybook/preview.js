export const parameters = {
  actions: {
    argTypesRegex: '^on[A-Z].*'
  },
  controls: {
    matchers: {
      date: /Date$/
    }
  },
  options: {
    storySort: {
      order: [
        'Install', [
          'Readme'
        ],
        'Map', [
          'Readme',
          'MMap',
          'MMarker',
          'MPopup',
          'MImage'
        ],
        'Sources',
        'Layers',
        'Controls'
      ]
    }
  }
}
